import random

from pywikibot import Claim, Site, Timestamp, WbTime


def create_claim(wikidata: Site, pid: str, target) -> Claim:
    claim = Claim(wikidata, pid)
    claim.setTarget(target)
    return claim


def parse_date(date: str) -> WbTime:
    time = WbTime.fromTimestamp(
        Timestamp.fromISOformat(date),
        precision=WbTime.PRECISION["day"],
    )
    time.hour = 0
    time.minute = 0
    time.second = 0
    return time


def today() -> WbTime:
    return parse_date(Timestamp.utcnow().isoformat())


def edit_group() -> str:
    return "([[:toolforge:editgroups/b/CB/{:x}|details]])".format(
        random.randrange(0, 2**48)
    )
