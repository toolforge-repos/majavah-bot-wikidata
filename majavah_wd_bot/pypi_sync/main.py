import argparse
import logging
from typing import Dict, List

import toolforge
import versions
from pywikibot import Claim, ItemPage, Site

from majavah_wd_bot.pypi_sync import pypi
from majavah_wd_bot.pypi_sync.pypi import get_data_from_pypi
from majavah_wd_bot.pypi_sync.wd import (
    ITEM_MOST_RECENT,
    ITEM_OBSOLETE,
    METADATA_PROPERTIES,
    PROPERTY_PYPI_PACKAGE,
    PROPERTY_REASON_PREFERRED,
    PROPERTY_RELEASE_DATE,
    PROPERTY_VERSION,
    create_source,
    has_pypi_source,
    is_pypi_source,
    query_items,
)
from majavah_wd_bot.utils import create_claim, edit_group

toolforge.set_user_agent("majavah-bot")


LOGGER = logging.getLogger(__name__)


def cli():
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true")
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)

    summary = f"update from PyPI {edit_group()}"

    LOGGER.info("Looking up Wikidata items")
    wikidata = Site("wikidata", "wikidata")

    for page in query_items():
        try:
            if len(page.get()["claims"][PROPERTY_PYPI_PACKAGE]) != 1:
                raise Exception("Not working with items with multiple packages")

            project_claim: Claim = page.get()["claims"][PROPERTY_PYPI_PACKAGE][0]
            pypi_project: str = project_claim.getTarget()
        except Exception as e:
            LOGGER.warning(
                "Unable extract PyPI project for %s", page.title(), exc_info=e
            )
            continue

        LOGGER.info("Processing %s %s", page.title(), pypi_project)

        try:
            project = get_data_from_pypi(pypi_project)
        except Exception as e:
            LOGGER.warning(
                "Unable to load data for PyPI project %s", pypi_project, exc_info=e
            )
            continue

        if pypi_project != project.name:
            if project.name != pypi.normalize(pypi_project):
                LOGGER.warning(
                    "PyPI project %s name is off", pypi_project, project.name
                )
                continue
            project_claim.changeTarget(project.name, summary=summary)
            pypi_project = project.name

        version_claims: Dict[str, Claim] = {}
        for claim in page.get()["claims"].get(PROPERTY_VERSION, []):
            version_claims[claim.getTarget()] = claim

        for release, release_data in project.releases.items():
            version = versions.parse_version(release)
            if version.is_dev_release() or version.is_pre_release():
                continue

            claim = version_claims.get(release)
            add_reference = False

            if not claim:
                if release_data.yanked:
                    continue

                LOGGER.info(
                    "Adding version %s on %s %s", release, pypi_project, page.title()
                )
                claim = create_claim(wikidata, PROPERTY_VERSION, release)
                add_reference = True

            if not claim.has_qualifier(PROPERTY_RELEASE_DATE, release_data.time):
                if len(claim.qualifiers.get(PROPERTY_RELEASE_DATE, [])) != 0:
                    LOGGER.warning(
                        "Found conflicting release date for %s %s",
                        pypi_project,
                        release,
                    )
                    continue

                add_reference = True

                LOGGER.info(
                    "Adding release date to release %s on %s %s",
                    release,
                    pypi_project,
                    page.title(),
                )

                claim.addQualifier(
                    create_claim(wikidata, PROPERTY_RELEASE_DATE, release_data.time),
                    summary=summary,
                )

            if claim.getRank() == "normal":
                if release_data.yanked:
                    claim.changeRank("deprecated", summary=summary)
                    # TODO: find a PROPERTY_REASON_DEPRECATED value for yanks
                    pass
                elif release == project.latest_version:
                    add_reference = True
                    LOGGER.info(
                        "Setting latest release %s on %s %s as preferred",
                        release,
                        pypi_project,
                        page.title(),
                    )

                    claim.setRank("preferred")
                    claim.addQualifier(
                        create_claim(
                            wikidata,
                            PROPERTY_REASON_PREFERRED,
                            ItemPage(wikidata, ITEM_MOST_RECENT),
                        ),
                        summary=summary,
                    )
            elif (
                claim.getRank() == "preferred"
                and release != project.latest_version
                and (
                    # Don't touch preferred state that was set by other bots.
                    len(claim.sources) == 0
                    or all(is_pypi_source(source) for source in claim.sources)
                )
            ):
                add_reference = True
                LOGGER.info(
                    "Removing preferred status from release %s on %s %s",
                    release,
                    pypi_project,
                    page.title(),
                )

                claim.setRank("normal")
                if PROPERTY_REASON_PREFERRED in claim.qualifiers:
                    claim.removeQualifiers(
                        claim.qualifiers[PROPERTY_REASON_PREFERRED], summary=summary
                    )

            if add_reference and not has_pypi_source(claim):
                LOGGER.info(
                    "Adding source to release %s on %s %s",
                    release,
                    pypi_project,
                    page.title(),
                )

                claim.addSources(
                    create_source(wikidata, project, release), summary=summary
                )

            if release not in version_claims:
                page.addClaim(claim, summary=summary)
                version_claims[release] = claim

        for url_name, pid in METADATA_PROPERTIES.items():
            if url_name not in project.project_urls:
                continue
            url = project.project_urls[url_name]

            all_claims: List[Claim] = page.get()["claims"].get(pid, [])
            matching_claims = [
                claim for claim in all_claims if claim.target_equals(url)
            ]

            for claim in all_claims:
                if claim in matching_claims:
                    continue
                if claim.getRank() != "normal":
                    continue

                if len(claim.sources) > 0 and all(
                    is_pypi_source(source) for source in claim.sources
                ):
                    LOGGER.info(
                        "Marking old %s (%s) as deprecated on %s %s",
                        url_name,
                        claim.getTarget(),
                        pypi_project,
                        page.title(),
                    )

                    claim.setRank("deprecated")
                    claim.addQualifier(
                        create_claim(
                            wikidata,
                            PROPERTY_REASON_PREFERRED,
                            ItemPage(wikidata, ITEM_OBSOLETE),
                        ),
                        summary=summary,
                    )

            if len(matching_claims) == 0:
                claim = create_claim(wikidata, pid, url)
                claim.addSources(create_source(wikidata, project, None))
                page.addClaim(claim, summary=summary)

                LOGGER.info(
                    "Creating %s (%s) on %s %s",
                    url_name,
                    claim.getTarget(),
                    pypi_project,
                    page.title(),
                )
                continue

            for claim in matching_claims:
                if not has_pypi_source(claim) and len(claim.sources) == 0:
                    LOGGER.info(
                        "Adding reference to existing %s (%s) on %s %s",
                        url_name,
                        claim.getTarget(),
                        pypi_project,
                        page.title(),
                    )

                    claim.addSources(
                        create_source(wikidata, project, None), summary=summary
                    )


if __name__ == "__main__":
    cli()
