import logging
import re
from dataclasses import dataclass
from typing import Dict, List

import requests
from pywikibot import WbTime

from majavah_wd_bot.utils import parse_date, today

LOGGER = logging.getLogger(__name__)


@dataclass
class Release:
    time: WbTime
    yanked: bool


@dataclass
class Project:
    name: str

    releases: Dict[str, Release]
    latest_version: str

    project_urls: Dict[str, str]
    classifiers: List[str]

    retrieved: WbTime


def get_data_from_pypi(project: str) -> Project:
    # https://peps.python.org/pep-0691/ is seriously broken :(
    # https://warehouse.pypa.io/api-reference/json.html#release

    response = requests.get(
        f"https://pypi.org/pypi/{project}/json",
        headers={"Accept": "application/json"},
    )
    response.raise_for_status()
    metadata = response.json()["info"]

    versions = {}
    for release_name, release_data in response.json()["releases"].items():
        sdists = [rel for rel in release_data if rel["packagetype"] == "sdist"]
        if len(sdists) != 1:
            LOGGER.warning("Invalid version %s in project %s", release_name, project)
            continue
        sdist = sdists[0]

        versions[release_name] = Release(
            time=parse_date(sdist["upload_time_iso_8601"]),
            yanked=sdist["yanked"],
        )

    if len(versions) == 0:
        raise Exception(f"No versions found for {project}")
    if metadata["version"] not in versions:
        raise Exception("Specified latest version is not available")

    return Project(
        name=metadata["name"],
        releases=versions,
        latest_version=metadata["version"],
        project_urls=metadata["project_urls"],
        classifiers=metadata["classifiers"],
        retrieved=today(),
    )


def normalize(name: str) -> str:
    # https://packaging.python.org/en/latest/specifications/name-normalization/
    return re.sub(r"[-_.]+", "-", name).lower()
