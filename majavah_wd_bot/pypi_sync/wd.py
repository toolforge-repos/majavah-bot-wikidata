from pathlib import Path
from typing import Dict, Iterator, List, Optional

from pywikibot import Claim, ItemPage, Site
from pywikibot.pagegenerators import WikidataSPARQLPageGenerator

from majavah_wd_bot.pypi_sync.pypi import Project
from majavah_wd_bot.utils import create_claim

PROPERTY_DOCUMENTATION = "P2078"
PROPERTY_ISSUE_TRACKER = "P1401"
PROPERTY_OFFICIAL_WEBSITE = "P1324"
PROPERTY_PYPI_PACKAGE = "P5568"
PROPERTY_REASON_DEPRECATED = "P2241"
PROPERTY_REASON_PREFERRED = "P7452"
PROPERTY_REFERENCE_URL = "P854"
PROPERTY_RELEASE_DATE = "P577"
PROPERTY_REPOSITORY = "P1324"
PROPERTY_RETRIEVED = "P813"
PROPERTY_TITLE = "P1476"
PROPERTY_VERSION = "P348"


ITEM_MOST_RECENT = "Q71533355"
ITEM_OBSOLETE = "Q107356532"


METADATA_PROPERTIES: Dict[str, str] = {
    "Bug Tracker": PROPERTY_ISSUE_TRACKER,
    "Documentation": PROPERTY_DOCUMENTATION,
    "Homepage": PROPERTY_OFFICIAL_WEBSITE,
    "Source": PROPERTY_REPOSITORY,
}


def query_items() -> Iterator[ItemPage]:
    return WikidataSPARQLPageGenerator(
        (Path(__file__).parent / "package_items.sq").read_text()
    )


def create_source(
    wikidata: Site, package: Project, version: Optional[str]
) -> List[Claim]:
    release = package.releases.get(version or package.latest_version)

    return [
        create_claim(
            wikidata,
            PROPERTY_REFERENCE_URL,
            (
                f"https://pypi.org/project/{package}/{version}/"
                if version
                else f"https://pypi.org/project/{package}/"
            ),
        ),
        create_claim(wikidata, PROPERTY_RELEASE_DATE, release.time),
        create_claim(wikidata, PROPERTY_RETRIEVED, package.retrieved),
        create_claim(
            wikidata,
            PROPERTY_TITLE,
            f"{package.name} {version}" if version else package.name,
        ),
    ]


def is_pypi_source(source: Dict[str, List[Claim]]) -> bool:
    for claim, values in source.items():
        if claim != PROPERTY_REFERENCE_URL:
            continue

        for value in values:
            if value.getTarget().startswith("https://pypi.org/"):
                return True
    return False


def has_pypi_source(claim: Claim) -> bool:
    for source in claim.getSources():
        if is_pypi_source(source):
            return True

    return False
